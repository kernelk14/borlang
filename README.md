# Borlang
A Programming Language for coping my stress

# Quick Start
If you have [get](https://gitlab.com/kernelk14/get) installed in your system:
```console
$ get -L kernelk14/borlang
```
Or cloning manually:
```console
$ git clone https://gitlab.com/kernelk14/borlang
```
Then, you will build the file.
```bash
$ ./build.sh
```