#!/usr/bin/env python3

import os
import getopt
import sys

filename = open("./wow.bl", "r")

program = filename.read().split()

def usage():
    print("""
Borlang Programming Language, created by Khyle Isaias at https://github.com/kernelk14

help:
    borlang [subcommand] <program>

    subcommands:
          -h    --help            Print this help message
""")

token = [
    'write',    # Token[0]
    'assert',   # Token[1]
    'var',      # Token[2]
]

stack = []
# Interpretation Stage.
def interpret(program):
    for p, op in enumerate(program):    # Program Loop
        # print(program)
        if program[p] == token[0]:      # Print Statement
            if program[p] == ' ':
                last_el = program[len(program) - 1]
                
                print(program[p+1].replace('"', "").join(last_el))
                # global last_el
                # print(program[p+1].replace('"', "").join(" ") + last_el)
                # print(f"Last element: {last_el}")
            else:
                print(program[p+1].replace('"', ""))
                # print(f"Last element: {last_el}")
        # print(token)
        if program[p] == token[1]:      # Assert
            print(f"Warning! Assertion at {filename.name.replace('./', '')}: " + program[p+1].replace('"', ''))
        if program[p] == token[2]:      # Var
            var_name = program[p+1]
            print(f"Variable Name: {var_name}")
            # print(var_name[:2])
            
            if program[p+1:1] == "=":
                print("Variable calling reached here.")
                value = program[p+1:2]
                print(value)
                stack = stack
                stack.append(value)
                # print(stack.pop())
def run(program):
    #  TODO: Make a run definition for the code.
    interpret(program)
# usage()
run(program)
